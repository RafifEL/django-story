from django.db import models

class Jadwal(models.Model):
    tanggal = models.DateField()
    jam = models.TimeField()
    nama_kegiatan = models.CharField(max_length= 100)
    tempat_kegiatan = models.CharField(max_length= 100)
    kategori = models.CharField(max_length= 100)
    hari = models.CharField(max_length=10, default = 'Senin')
