from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'time'

class FormJadwal(forms.Form):
    kategori = forms.CharField(max_length= 100, required=True)
    nama_kegiatan = forms.CharField(max_length= 100, required=True)
    tanggal = forms.DateField(
        required=True, 
        widget=DateInput)
    jam = forms.TimeField(
        required = True,
        widget=TimeInput,)
    tempat_kegiatan = forms.CharField(max_length= 100, required=True)
    
    
