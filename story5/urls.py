"""ppw_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'story5'
urlpatterns = [
    path('', views.home, name='home' ),
    path('tentang-diriku', views.tentang_diriku, name='about'),
    path('pengalaman', views.pengalaman, name="exp"),
    path('under-construction', views.under_construction, name="under"),
    path('jadwal', views.form_jadwal, name= 'jadwal'),
    path('jadwal-delete/<id>/', views.delete_jadwal, name = 'jadwal-delete')
]
