from django.shortcuts import render, redirect
from .forms import FormJadwal
from .models import Jadwal

def home(request):
    return render(request, "home.html")

def tentang_diriku(request):
    return render(request, "tentang_diriku.html")

def pengalaman(request):
    return render(request, "pengalaman.html")

def under_construction(request):
    return render(request, "under_construction.html")

def form_jadwal(request):
    if request.method == "POST":
        form = FormJadwal(request.POST)
        if form.is_valid():
            nama_kegiatan = form.cleaned_data['nama_kegiatan']
            kategori = form.cleaned_data['kategori']
            tanggal = form.cleaned_data['tanggal']
            jam = form.cleaned_data['jam']
            tempat = form.cleaned_data['tempat_kegiatan']
            hari = None
            if tanggal.strftime("%A") == "Monday":
                hari = "Senin"
            elif tanggal.strftime("%A") == "Tuesday":
                hari = "Selasa"
            elif tanggal.strftime("%A") == "Wednesday":
                hari = "Rabu"
            elif tanggal.strftime("%A") == "Thursday":
                hari = "Kamis"
            elif tanggal.strftime("%A") == "Friday":
                hari = "Jumat"
            elif tanggal.strftime("%A") == "Saturday":
                hari = "Sabtu"
            elif tanggal.strftime("%A") == "Sunday":
                hari = "Minggu"

            jadwal = Jadwal(
                nama_kegiatan = nama_kegiatan,
                kategori = kategori,
                tanggal = tanggal,
                jam = jam,
                tempat_kegiatan = tempat,
                hari = hari)
            jadwal.save()
            Jadwal.objects.order_by('-tanggal')
        return redirect("story5:jadwal")
    else:
        form = FormJadwal()
        return render(request, "form_jadwal.html", {"form" : form, "list_jadwal": Jadwal.objects.all()})

def delete_jadwal(request, id):
    jadwal = Jadwal.objects.get(id=id)
    jadwal.delete()
    return redirect("story5:jadwal")



